#!/usr/bin/python

# python challenge
# url = http://www.pythonchallenge.com/pc/hex/idiot2.html

# 0 2**38
#_0 = 274877906944
# 1 g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj.
# _1 = jvvr://yyy.ravjqpejcnngpig.eqo/re/fgh/ocr.jvon
# 2 ocr.txt
# _2 EQUALITY
# 3 bodyguards.txt
#_3 = linkedlist
# 4 next ?nothing=xxxx
# _4 final nothing = 66831 => peak.html
# 5 pickle
# _5 unpickle the pickle... => channel
# 6 now there are zips
# _6 hockey -> OXYGEN
# 7 oxygen.png colors
# _7 integrity
# 8 decompress 2 bz2 strings
# _8 :huge:file
# 9 connect the dots
# _9 PIL draw lines => bull
# 10 Look and say
# _10 5808
# 11 Odd even
# _11 even => evil
# 12 5 images in .gtx
# _12 555-ITALY => italy.html
# 13 call the evil
# _13 xmlrpc call Bert
# 14 itally spiral wire
# _14 rebuild image 100 * 100
# 15 leap year, when 26 jan 1XX6 was on monday
# _15 mozart
# 16 let me get this straight
# _16 align image with purple at beginning => romance
# 17 level 4 cookie, busynothing search, get all cookies, decompress them as bz2, call Leopold, add cookie "info=the flowers are on their way;"
# _17 balloons.html
# 18 differeces = brightness => brightness.html => deltas.gz => difflib => 3 images, ../hex/bin.html, user: butter, pass: fly
# _18 hex/bin.html (butter:fly)
# 19 double the framerates => "you are an idiost hahahaha" => idiot.html
# 20 image is only 30202 bytes of the total resp lengh... 30203 and on => text, resp lenght -- = zip passwd and location (reverse seach twice)
# 21 zip downloaded from 20

import sys
import difflib
if sys.version_info >= (3, 0):  # python 3
    import re
    import requests
    import pickle
    import zipfile
    import bz2
    import xmlrpc.client
    import os
    import calendar
    import time
    import numpy
    import binascii
    import base64
    import struct
    from datetime import date
    # from PIL import Image, ImageDraw
else:  # python 2
    import bz2
    import urllib
    import requests
    import re
    import xmlrpclib
    from PIL import Image, ImageDraw


def _0():
    print(2 ** 38)


# _0()

def _1():
    az = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
          "w", "x", "y", "z", "a", "b"]
    enc = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb r\
        fyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
    url = "http://www.pythonchallenge.com/pc/def/map.html"
    dec = ""
    for i in url:
        if i in az:
            dec += az[az.index(i) + 2]
        else:
            dec += i
    print(dec)

# _1()


def _2():
    f = open("ocr.txt", "r")
    data = f.read()
    diff_chars = {}
    for i in data:
        if i in diff_chars:
            diff_chars[i] += 1
        else:
            diff_chars[i] = 1

    print(diff_chars)

# _2()


def _3():
    f = open("bodyguards.txt", "r")
    data = f.read()
    new_data = ""
    for i in data:
        new_data += i.strip("\n")

    if True:
        print(re.findall(
            "[a-z]{1}[A-Z]{3}([a-z]{1})[A-Z]{3}[a-z]{1}", new_data))
    else:
        for i in range(4, len(new_data) - 3):
            if new_data[i - 1].isupper() and new_data[i - 2].isupper() and new_data[i - 3].isupper() \
                    and new_data[i].islower() and new_data[i + 1].isupper() and new_data[i + 2].isupper() and new_data[
                        i + 3].isupper():
                if i > 4 or i < len(new_data) - 3:
                    if new_data[i - 4].islower() and new_data[i + 4].islower():
                        print(new_data[i])

# _3()


def _4():
    url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="
    # from 12345 => 16044 - "Yes. Divide by two and keep going."
    nothing = str(16044 / 2)
    search = True
    while search:
        html = requests.get(url + nothing).text
        print("-" * 40)
        print(html)
        match = re.search("([0-9]+)$", html)
        if match is not None:
            print(match.group(0))
            nothing = match.group(0)
        else:
            search = False
        print("-" * 40)

# _4()


def _5():
    with open("banner.p", "rb") as f:
        data = pickle.load(f)

    for line in data:
        print("".join(c * n for c, n in line))

# _5()


def _6():
    folder = "channel/"
    nothing = "90052.txt"
    zipf = zipfile.ZipFile("channel.zip")
    comments = []

    while True:
        with open(folder + nothing, "r") as f:
            data = f.read()

        match = re.search("(\d+)$", data)
        print(nothing + " \t=>\t " + data)

        comments.append(zipf.getinfo(nothing).comment.decode("UTF-8"))

        if match is not None:
            nothing = match.group(1) + ".txt"
        else:
            break

    print("".join(comments))

# _6()


def _7():
    img = Image.open("oxygen.png", "r")
    width, height = img.size
    pixel_values = img.load()
    colors = []

    for i in range(0, width, 7):
        colors.append(chr(pixel_values[i, 47][0]))

    print("".join(colors))

    next_lvl = re.findall("\d{3}", "".join(colors))
    for l in next_lvl:
        print(chr(int(l)))

# _7()


def _8():
    un = b"BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 \x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084"
    pw = b"BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ \x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08"
    print(bz2.decompress(un))
    print(bz2.decompress(pw))


# _8()

def _9():
    first = [146, 399, 163, 403, 170, 393, 169, 391, 166, 386, 170, 381, 170, 371, 170, 355, 169, 346, 167, 335, 170,
             329, 170, 320, 170, 310, 171, 301, 173, 290, 178, 289, 182, 287, 188, 286, 190, 286, 192, 291, 194, 296,
             195, 305, 194, 307, 191, 312, 190, 316, 190, 321, 192, 331, 193, 338, 196, 341, 197, 346, 199, 352, 198,
             360, 197, 366, 197, 373, 196, 380, 197, 383, 196, 387, 192, 389, 191, 392, 190, 396, 189, 400, 194, 401,
             201, 402, 208, 403, 213, 402, 216, 401, 219, 397, 219, 393, 216, 390, 215, 385, 215, 379, 213, 373, 213,
             365, 212, 360, 210, 353, 210, 347, 212, 338, 213, 329, 214, 319, 215, 311, 215, 306, 216, 296, 218, 290,
             221, 283, 225, 282, 233, 284, 238, 287, 243, 290, 250, 291, 255, 294, 261, 293, 265, 291, 271, 291, 273,
             289, 278, 287, 279, 285, 281, 280, 284, 278, 284, 276, 287, 277, 289, 283, 291, 286, 294, 291, 296, 295,
             299, 300, 301, 304, 304, 320, 305, 327, 306, 332, 307, 341, 306, 349, 303, 354, 301, 364, 301, 371, 297,
             375, 292, 384, 291, 386, 302, 393, 324, 391, 333, 387, 328, 375, 329, 367, 329, 353, 330, 341, 331, 328,
             336, 319, 338, 310, 341, 304, 341, 285, 341, 278, 343, 269, 344, 262, 346, 259, 346, 251, 349, 259, 349,
             264, 349, 273, 349, 280, 349, 288, 349, 295, 349, 298, 354, 293, 356, 286, 354, 279, 352, 268, 352, 257,
             351, 249, 350, 234, 351, 211, 352, 197, 354, 185, 353, 171, 351, 154, 348, 147, 342, 137, 339, 132, 330,
             122, 327, 120, 314, 116, 304, 117, 293, 118, 284, 118, 281, 122, 275, 128, 265, 129, 257, 131, 244, 133,
             239, 134, 228, 136, 221, 137, 214, 138, 209, 135, 201, 132, 192, 130, 184, 131, 175, 129, 170, 131, 159,
             134, 157, 134, 160, 130, 170, 125, 176, 114, 176, 102, 173, 103, 172, 108, 171, 111, 163, 115, 156, 116,
             149, 117, 142, 116, 136, 115, 129, 115, 124, 115, 120, 115, 115, 117, 113, 120, 109, 122, 102, 122, 100,
             121, 95, 121, 89, 115, 87, 110, 82, 109, 84, 118, 89, 123, 93, 129, 100, 130, 108, 132, 110, 133, 110, 136,
             107, 138, 105, 140, 95, 138, 86, 141, 79, 149, 77, 155, 81, 162, 90, 165, 97, 167, 99, 171, 109, 171, 107,
             161, 111, 156, 113, 170, 115, 185, 118, 208, 117, 223, 121, 239, 128, 251, 133, 259, 136, 266, 139, 276,
             143, 290, 148, 310, 151, 332, 155, 348, 156, 353, 153, 366, 149, 379, 147, 394, 146, 399]

    second = [156, 141, 165, 135, 169, 131, 176, 130, 187, 134, 191, 140, 191, 146, 186, 150, 179, 155, 175, 157, 168,
              157, 163, 157, 159, 157, 158, 164, 159, 175, 159, 181, 157, 191, 154, 197, 153, 205, 153, 210, 152, 212,
              147, 215, 146, 218, 143, 220, 132, 220, 125, 217, 119, 209, 116, 196, 115, 185, 114, 172, 114, 167, 112,
              161, 109, 165, 107, 170, 99, 171, 97, 167, 89, 164, 81, 162, 77, 155, 81, 148, 87, 140, 96, 138, 105, 141,
              110, 136, 111, 126, 113, 129, 118, 117, 128, 114, 137, 115, 146, 114, 155, 115, 158, 121, 157, 128, 156,
              134, 157, 136, 156, 136]

    img = Image.new("RGB", (512, 512), (255, 255, 255))
    img_draw = ImageDraw.Draw(img)
    img_draw.line(first, fill=(0, 0, 0))
    img_draw.line(second, fill=(0, 0, 0))
    img.show()


# _9()

def _10():
    # 1, 11, 21, 1211, 111221
    seq = "1"
    for i in range(30):
        match = re.findall(r"(\d)(\1*)", seq)
        seq = "".join([str(len(x + y)) + x for x, y in match])

    print(len(seq))

# _10()


def _11():
    img = Image.open("cave.jpg")
    img_final = Image.new("RGB", (640, 480), (0, 0, 0))
    img_draw = ImageDraw.Draw(img_final)
    width, height = img.size
    pixel_values = img.load()

    for x in range(0, width, 2):
        for y in range(0, height, 2):
            img_draw.point((x, y), pixel_values[x, y])

    img_final.show()

# _11()


def _12():
    gfx = open("evil2.gfx", "rb")
    evil_1 = open("evil_1.png", "wb")
    evil_2 = open("evil_2.png", "wb")
    evil_3 = open("evil_3.png", "wb")
    evil_4 = open("evil_4.png", "wb")
    evil_5 = open("evil_5.png", "wb")

    for i in range(67575):
        evil_1.write(gfx.read(1))
        evil_2.write(gfx.read(1))
        evil_3.write(gfx.read(1))
        evil_4.write(gfx.read(1))
        evil_5.write(gfx.read(1))

    gfx.close()
    evil_1.close()
    evil_2.close()
    evil_3.close()
    evil_4.close()
    evil_5.close()

# _12()


def _13():
    s = xmlrpc.client.ServerProxy(
        "http://www.pythonchallenge.com/pc/phonebook.php")
    print(s.system.listMethods())
    print(s.system.methodHelp("phone"))
    print(s.phone("Bert"))

# _13()


def _14():
    img = Image.open("wire.png")
    delta = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    x, y, p = -1, 0, 0
    img_final = Image.new("RGB", (100, 100), (255, 255, 255))
    img_draw = ImageDraw.Draw(img_final)
    d = 200
    while d / 2 > 0:
        for v in delta:
            steps = d // 2
            for s in range(steps):
                x, y = x + v[0], y + v[1]
                img_final.putpixel((x, y), img.getpixel((p, 0)))
                p += 1
            d -= 1

    img_final.show()

# _14()


def _15():
    for i in range(1500, 2017):
        d = date(i, 1, 26)
        if d.weekday() == 0 \
            and str(d.year)[-1] == "6" \
                and calendar.isleap(d.year):
            print(d)

# _15()


def _16():
    img = Image.open("mozart.gif")
    img = img.convert("RGB")
    width, height = img.size
    pixel_values = img.load()

    img_final = Image.new("RGB", img.size, (255, 255, 255))
    img_draw = ImageDraw.Draw(img_final)

    purple = (255, 0, 255)

    for y in range(0, height):
        for x in range(0, width):
            if pixel_values[x, y] == purple:

                for i in range(0, width - x):
                    img_final.putpixel((i, y), pixel_values[x - 1 + i, y])

                for i in range(width - x, width):
                    img_final.putpixel(
                        (i, y), pixel_values[i - (width - x), y])

                break

    img_final.show()

# _16()


def _17():
    if sys.version_info >= (3, 0):
        print("Requires Python 2.x, not Python 3.x")
        sys.exit(1)

    url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?busynothing="
    nothing = "12345"  # => 83051
    jar = ""
    search = True
    while search:
        resp = requests.get(url + nothing)
        html = resp.text
        cookie = resp.cookies["info"]
        jar = jar + cookie
        print("-" * 40)
        print(html)
        match = re.search("([0-9]+)$", html)
        if match is not None:
            print(match.group(0))
            nothing = match.group(0)
        else:
            search = False
        print("-" * 40)

    print(jar)
    # => "is it the 26th already? call his father and inform him that "the flowers are on their way". he'll understand." => Mozart's father => Leopold
    print(bz2.decompress(urllib.unquote_plus(jar)))

    s = xmlrpclib.ServerProxy(
        "http://www.pythonchallenge.com/pc/phonebook.php")
    print(s.system.listMethods())
    print(s.system.methodHelp("phone"))
    # => /pc/stuff/violin.php, add document.cookie="info=the flowers are on their way;" => balloons.html
    print(s.phone("Leopold"))

# _17()


def _18():
    with open("deltas", "r") as f:
        data = f.readlines()

    seq_1 = []
    seq_2 = []
    for line in data:
        seq_1.append(line[:55].strip())
        seq_2.append(line[55:].strip())

    differ = difflib.Differ()
    diff = list(differ.compare(seq_1, seq_2))

    img_0 = open("diff_0.png", "wb")
    img_1 = open("diff_1.png", "wb")
    img_2 = open("diff_2.png", "wb")

    for d in diff:
        bytes_seq = [chr(int(b, 16)) for b in d[2:].split()]

        if d.startswith(" "):
            for b in bytes_seq:
                img_0.write(b)

        if d.startswith("-"):
            for b in bytes_seq:
                img_1.write(b)

        if d.startswith("+"):
            for b in bytes_seq:
                img_2.write(b)

# _18()


def _19():
    b64 = None
    with open("indian.b64", "r") as f:
        b64 = f.read()
    wav = base64.b64decode(b64)
    s = len(wav[44:]) // 2
    res = wav[:44]
    for i in range(s):
        tmp = struct.unpack("<h", wav[44 + 2 * i: 44 + 2 * i + 2])
        print(tmp)
        res = res + struct.pack(">h", tmp[0])
    with open("indian.wav", "wb") as f:
        f.write(res)

# _19()


def _20():
    resps = []
    url = "http://butter:fly@www.pythonchallenge.com/pc/hex/unreal.jpg"
    r = requests.get(url, stream=True)
    start, next, end = re.search(r"(\d+)-(\d+)/(\d+)", r.headers["Content-Range"]).groups()
    start = int(start)
    next = 1152983631
    end = int(end)
    while next < end:
        headers = {"Range": "bytes=%d-/2123456789" % next}
        r = requests.get(url, headers=headers, stream=True)
        try:
            start, next, end = re.search(r"(\d+)-(\d+)/(\d+)", r.headers["Content-Range"]).groups()
            start = int(start)
            next = int(next) + 1
            end = int(end)
        except:
            next = next + 1
        
        print(start, next, end)
        if (len(r.text) > 0):
            with open("fence.zip", "wb") as f:
                f.write(r.content)
            resps.append(r.text)
        print("-" * 40)

    print("=" * 40)
    print("\n".join(resps))

_20()
